from prepare_data import *
from build_models import *
from tests import *


if __name__ == "__main__":
    print("preparing data...")
    X_train = np.loadtxt('data/X_train.txt')
    X_test = np.loadtxt('data/X_test.txt')
    y_train = np.loadtxt('data/y_train.txt')
    y_test = np.loadtxt('data/y_test.txt')
    new_X_train = np.loadtxt('data/new_X_train.txt')
    new_X_test = np.loadtxt('data/new_X_test.txt')
    new_y_train = np.loadtxt('data/new_y_train.txt')
    new_y_test = np.loadtxt('data/new_y_test.txt')
    if np.size(X_train) == 0 or np.size(X_test) == 0 or np.size(
            y_train) == 0 or np.size(y_test) == 0:
        X_train, X_test, y_train, y_test, new_X_train, new_X_test, new_y_train, new_y_test = prepare_data(
            'data/Norweydata.csv')
    else:
    	print("took data from backup")

    print("building regression models...")
    if not (
        os.path.isfile('models/model_linreg.sav')) or not (
        os.path.isfile('models/model_forest.sav')) or not (
            os.path.isfile('models/model_xgb_r.sav')):
        build_regression_models(X_train, y_train)
    else:
    	print("took regression models from backup")

    print("building classify models...")
    if not (os.path.isfile('models/model_xgb_c.sav')):
        build_classify_models(X_train, y_train, new_X_train, new_y_train)
    else:
    	print("took classify models from backup")

    print("testing regression models...")
    test_regression_models(X_test, y_test)
    print("testing classify models...")
    test_classify_models(X_test, y_test, new_X_test, new_y_test)

    print("done!")
    print("view data at data/\n     models at models/\n     results at results/")
