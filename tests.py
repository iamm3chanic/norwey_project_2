import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import joblib
import os

from sklearn.model_selection import GridSearchCV, TimeSeriesSplit, cross_val_score
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score


def test_regression_models(X_test, y_test):
    # загружаем модели
    model1 = joblib.load('models/model_linreg.sav')
    model2 = joblib.load('models/model_forest.sav')
    model3 = joblib.load('models/model_xgb_r.sav')

    y_lr = model1.predict(X_test)
    y_pred_forest = model2.predict(X_test)
    y_xgb = model3.predict(X_test)

    # считаем погрешности
    y_true = y_test

    print('mae linreg = ', mean_absolute_error(y_true, y_lr))
    print('mae randfor = ', mean_absolute_error(y_true, y_pred_forest))
    print('mae xgboost = ', mean_absolute_error(y_true, y_xgb))
    print('mse linreg = ', mean_squared_error(y_true, y_lr))
    print('mse randfor = ', mean_squared_error(y_true, y_pred_forest))
    print('mse xgboost = ', mean_squared_error(y_true, y_xgb))

    if os.path.getsize("results/regression-results.txt") == 0:
	    with open("results/regression-results.txt", "w") as file_:
	        print('mae linreg = ', mean_absolute_error(y_true, y_lr), file=file_)
	        print(
	            'mae randfor = ',
	            mean_absolute_error(
	                y_true,
	                y_pred_forest),
	            file=file_)
	        print('mae xgboost = ', mean_absolute_error(y_true, y_xgb))
	        print(
	            'mse linreg = ',
	            mean_squared_error(
	                y_true,
	                y_lr),
	            file=file_)
	        print(
	            'mse randfor = ',
	            mean_squared_error(
	                y_true,
	                y_pred_forest),
	            file=file_)
	        print(
	            'mse xgboost = ',
	            mean_squared_error(
	                y_true,
	                y_xgb),
	            file=file_)
	        print("\n", file=file_)
	        print("y_linreg y_randforest y_xgboost", file=file_)
	        for i in range(len(y_lr)):
	            print(y_lr[i], y_pred_forest[i], y_xgb[i], file=file_)


def test_classify_models(X_test, y_test, new_X_test, new_y_test):
    # загружаем модели
    #model1 = joblib.load('models/model_knn.sav')
    model2 = joblib.load('models/model_logreg.sav')
    model3 = joblib.load('models/model_xgb_c.sav')

    #KNN_prediction = model1.predict(X_test)
    LogReg_prediction = model2.predict(X_test)
    # возможно следует делать тесты на укороченном
    XGB_prediction = model3.predict(new_X_test)

    # Оценка точности — простейший вариант оценки работы классификатора
    #print("accuracy_knn =", accuracy_score(KNN_prediction, y_test))
    print("accuracy_logreg =", accuracy_score(LogReg_prediction, y_test))
    print("accuracy_xgb =", accuracy_score(XGB_prediction, new_y_test))
    # матрица неточности и отчёт о классификации дадут больше информации о
    # производительности
    #print("confusion_matrix KNN:\n", confusion_matrix(KNN_prediction, y_test))
    print("confusion_matrix LogReg:\n",confusion_matrix(LogReg_prediction,y_test))
    print("confusion_matrix XGB:\n", confusion_matrix(XGB_prediction, new_y_test))

    #print("classification_report KNN: \n",classification_report(KNN_prediction,y_test))
    print("classification_report LogReg: \n",classification_report(LogReg_prediction, y_test))
    print(
        "classification_report XGB: \n",
        classification_report(
            XGB_prediction,
            new_y_test))
    
    if os.path.getsize("results/classify-results.txt") == 0:
	    with open("results/classify-results.txt", "w") as file_:
	        #print("accuracy_knn =",accuracy_score(KNN_prediction,y_test),file=file_)
	        print("accuracy_logreg =",accuracy_score(LogReg_prediction,y_test),file=file_)
	        print(
	            "accuracy_xgb =",
	            accuracy_score(
	                XGB_prediction,
	                new_y_test),
	            file=file_)
	        # матрица неточности и отчёт о классификации дадут больше
	        # информации о производительности
	        #print("confusion_matrix KNN:\n",confusion_matrix(KNN_prediction,y_test),file=file_)
	        print("confusion_matrix LogReg:\n",confusion_matrix(LogReg_prediction,y_test),file=file_)
	        print(
	            "confusion_matrix XGB:\n",
	            confusion_matrix(
	                XGB_prediction,
	                new_y_test),
	            file=file_)

	        #print("classification_report KNN: \n",classification_report(KNN_prediction, y_test), file=file_)
	        print("classification_report LogReg: \n",classification_report(LogReg_prediction, y_test), file=file_)
	        print(
	            "classification_report XGB: \n",
	            classification_report(
	                XGB_prediction,
	                new_y_test),
	            file=file_)

	        print("\n", file=file_)
	        print("y_logreg y_xgb", file=file_)
	        for i in range(len(XGB_prediction)):
	            print(
	                #KNN_prediction[i],
	                LogReg_prediction[i],
	                XGB_prediction[i],
	                file=file_)
