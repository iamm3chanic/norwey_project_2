import pandas as pd
import numpy as np
import csv
import joblib

from sklearn.model_selection import GridSearchCV, TimeSeriesSplit, cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor, XGBClassifier


def build_regression_models(X_train, y_train):
    # Читаем данные из текстовых файлов
    if np.size(X_train) == 0 or np.size(y_train) == 0:
        X_train = np.loadtxt('data/X_train.txt')
        y_train = np.loadtxt('data/y_train.txt')
    # print(X_train[0])

    # Обучаем линейную регрессию
    print("Обучаем линейную регрессию (это быстро)")
    model1 = LinearRegression()
    model1.fit(X_train, y_train)
    # сохраняем
    joblib.dump(model1, 'models/model_linreg.sav')

    # Обучаем рандомные леса
    print("Обучаем рандомные леса (это долго)")
    model2 = RandomForestRegressor(n_estimators=5, max_depth=5)
    model2.fit(X_train, y_train)
    # сохраняем
    joblib.dump(model2, 'models/model_forest.sav')

    '''
    # Обучаем SVR
    print("Обучаем SVR (это нереально долго)")
    model = SVR(kernel='rbf')
    model.fit(X_train, y_train)
    y_svr = model.predict(X_test)
    '''

    # Обучаем градиентный бустинг
    print("Обучаем xgboost (это очень долго)")
    model3 = XGBRegressor()
    model3.fit(X_train, y_train)
    # сохраняем
    joblib.dump(model3, 'models/model_xgb_r.sav')
    


def build_classify_models(X_train, y_train, new_X_train, new_y_train):
    # Читаем данные из текстовых файлов
    if np.size(X_train) == 0 or np.size(y_train) == 0 or np.size(
            new_X_train) == 0 or np.size(new_y_train) == 0:
        X_train = np.loadtxt('data/X_train.txt')
        y_train = np.loadtxt('data/y_train.txt')
        new_X_train = np.loadtxt('data/new_X_train.txt')
        new_y_train = np.loadtxt('data/new_y_train.txt')
    #print(X_train[0])
    # метод опорных векторов
    # SVC_model = SVC()
    # метод k-ближайших соседей
    # В KNN-модели нужно указать параметр n_neighbors
    KNN_model = KNeighborsClassifier(n_neighbors=3)
    # логистическая регрессия
    logisticRegr = LogisticRegression(solver='lbfgs', max_iter=100)
    # градиентный бустинг
    XGB_model = XGBClassifier()
    
    #print("Обучаем SVC  (это очень долго)")
    #SVC_model.fit(X_train, y_train)
    # сохраняем
    #joblib.dump(SVC_model, 'model_svc.sav')

    print("Обучаем KNN (это быстро)")
    KNN_model.fit(X_train, y_train)
    joblib.dump(KNN_model, 'models/model_knn.sav')
    print("Обучаем логистическую регрессию (это долго и ломается)")
    logisticRegr.fit(X_train, y_train)
    joblib.dump(logisticRegr, 'models/model_logreg.sav')
    
    print("Обучаем xgboost (это очень долго, делаем на укороченном датасете)")
    XGB_model.fit(new_X_train, new_y_train)
    joblib.dump(XGB_model, 'models/model_xgb_c.sav')
   
'''
logreg error:

/home/rommeldaughter/.local/lib/python3.10/site-packages/sklearn/linear_model/_logistic.py:444: ConvergenceWarning: lbfgs failed to converge (status=1):
STOP: TOTAL NO. of ITERATIONS REACHED LIMIT.

Increase the number of iterations (max_iter) or scale the data as shown in:
    https://scikit-learn.org/stable/modules/preprocessing.html
Please also refer to the documentation for alternative solver options:
    https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression
  n_iter_i = _check_optimize_result(
'''
