import pandas as pd
import numpy as np
import csv

# from mlutils.transformers.generic import MultiColumnLabelEncoder
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split


def prepare_data(filename):
    # Загружаем данные
    df = pd.read_csv(filename, sep=';')
    '''
    WELL;DEPTH_MD;X_LOC;Y_LOC;Z_LOC;GROUP;FORMATION;CALI;RSHA;RMED;RDEP;
    RHOB;GR;SGR;NPHI;PEF;DTC;SP;BS;ROP;DTS;DCAL;DRHO;MUDWEIGHT;RMIC;
    ROPA;RXO;FORCE_2020_LITHOFACIES_LITHOLOGY;
    FORCE_2020_LITHOFACIES_CONFIDENCE
    '''

    # предобработка nan в результатах
    df = df.dropna(subset=['FORMATION'])
    #df = df.fillna(0) - not exact
    #df = df.fillna(df.mean()) - too slow & not exact

    # проверяем сколько уникальных значений есть
    print(df['WELL'].unique())
    print(df['FORCE_2020_LITHOFACIES_LITHOLOGY'].unique())
    print(df['FORMATION'].unique())
    print(df.shape) # 1033513 x 29

    # группировка по источнику и результату
    group = df.groupby(['WELL', 'FORMATION'])


    # lag (prev features)
    lag_start = 1
    lag_end = 5
    for i in range(lag_start, lag_end):
        df['lag_{}'.format(i)] = df.FORMATION.shift(i)
    for i in range(lag_start, lag_end):
        df = df.dropna(subset=['lag_{}'.format(i)])

    print(df.head())


    # числовые фичи
    # все
    num_features = ['DEPTH_MD', 'X_LOC', 'Y_LOC', 'Z_LOC', 'CALI', 
                    'RSHA', 'RMED', 'RDEP', 'RHOB', 'GR', 'SGR', 'NPHI',
                    'PEF', 'DTC', 'SP', 'BS', 'ROP', 'DTS', 'DCAL', 
                    'DRHO', 'MUDWEIGHT', 'RMIC', 'ROPA', 'RXO']
    # без редких
    for column in num_features:
        #print("nan in", column, df[column].isna().sum())
        if df[column].isna().sum() > 150000:
            #print('not OK')
            num_features.remove(column)

    # дополним до not nan методом forward fill, а после - backward
    df = df.fillna(method="ffill")
    df = df.fillna(method="bfill")

    df_n = df[num_features]

    # категориальные фичи
    # Используем Label-Encoding для названий
    le = LabelEncoder()
    
    # столбец результатов тоже закодируем по категориям
    y = df['FORMATION']
    label_encoder = le.fit(y)
    y = label_encoder.transform(y)
    
    cat_features = ['WELL', 'FORCE_2020_LITHOFACIES_CONFIDENCE', 
                    'GROUP', 'FORCE_2020_LITHOFACIES_LITHOLOGY',
                    'lag_1', 'lag_2', 'lag_3', 'lag_4']

    # соберем датафрейм обратно из числовых и категориальных фич и у
    df = pd.concat((df_n, df[cat_features].apply(le.fit_transform)), axis=1)
    
    df['FORMATION'] = y
    print(df.shape, df_n.shape, '\nlen y =', len(y))
    print(df.head())

    # Отделяем значения признаков от результата (у - результат)
    # Разбиваем данные на train и test

    X = df.drop('FORMATION', axis=1)

    train_df, test_df = train_test_split(df, test_size=0.25)

    print("train", train_df.head())
    print("test", test_df.head())

    # таргеты
    y_train = train_df.FORMATION
    y_test = test_df.FORMATION

    # категориальные фичи
    X_train_cat = train_df[cat_features].values
    X_test_cat = test_df[cat_features].values

    # скалируем числовые фичи
    X_train_num = train_df[num_features].copy()
    X_test_num = test_df[num_features].copy()

    scaler = StandardScaler()

    X_train_num = scaler.fit_transform(X_train_num)
    X_test_num = scaler.transform(X_test_num)

    # итоговые датасеты для обучения
    X_train = np.hstack((X_train_cat, X_train_num))
    X_test = np.hstack((X_test_cat, X_test_num))

    # сохраним это в файлы
    np.savetxt('data/X_train.txt', X_train)
    np.savetxt('data/X_test.txt', X_test)
    np.savetxt('data/y_train.txt', y_train)
    np.savetxt('data/y_test.txt', y_test)

    # хотим выборку поменьше для xgb, чтобы он не обучался так долго
    
    new_size = round(len(df) / len(df['FORMATION'].unique()) / 200)
    print("new size =", new_size)
    new_df = pd.DataFrame(columns=['DEPTH_MD', 'X_LOC', 'Y_LOC', 'Z_LOC', 'CALI', 
                    'RSHA', 'RMED', 'RDEP', 'RHOB', 'GR', 'SGR', 'NPHI',
                    'PEF', 'DTC', 'SP', 'BS', 'ROP', 'DTS', 'DCAL', 
                    'DRHO', 'MUDWEIGHT', 'RMIC', 'ROPA', 'RXO',
                    'WELL', 'FORCE_2020_LITHOFACIES_CONFIDENCE', 
                    'GROUP', 'FORCE_2020_LITHOFACIES_LITHOLOGY',
                    'lag_1', 'lag_2', 'lag_3', 'lag_4', 'FORMATION'])
    
    # проход по количеству уникальных значений формаций
    # работает очень долго!
    '''
    for i in range(len( df['FORMATION'].unique() )):
        formation_name = df['FORMATION'].unique()[i] 
        counter = 0
        # проход по строкам датасета
        for index, row in df.iterrows():
            # если найдена наша формация
            if row['FORMATION'] == formation_name:
                # append a string
                new_df = pd.concat([new_df, row], ignore_index=True)
                counter = counter + 1
            # пока не счетчик не стал равен нужной нам длине
            if counter >= new_size :
                df = df[df.FORMATION != formation_name]
                break
        print(formation_name, counter)
    '''

    df = df.sort_values(by=['FORMATION'])
    print("sorted", df.head())
    old_name = 0
    counter = 0
    new_arr = []
    for index, row in df.iterrows():
        new_name = row['FORMATION']
        if counter == new_size and new_name == old_name:
            # df = df[df.FORMATION != new_name]
            old_name = new_name
            continue
        # если совпадают результаты, прикрепим строчку
        if counter < new_size and new_name == old_name:
            counter = counter + 1
            # slow bc of this!
            #new_df = pd.concat([new_df, row], ignore_index=True)
            new_arr.append(row)
        else:
            counter = 0
            print("wrote", old_name)
        old_name = new_name
    new_df = pd.concat([new_df, pd.DataFrame(new_arr)], axis=0, ignore_index=True)
    del new_arr
    new_X = new_df.drop('FORMATION', axis=1)
    new_y = new_df['FORMATION']

    new_train_df, new_test_df = train_test_split(new_df, test_size=0.25)

    print("train", new_train_df.head())
    print("test", new_test_df.head())

    # таргеты
    new_y_train = new_train_df.FORMATION
    new_y_test = new_test_df.FORMATION

    # категориальные фичи
    new_X_train_cat = new_train_df[cat_features].values
    new_X_test_cat = new_test_df[cat_features].values

    # скалируем числовые фичи
    new_X_train_num = new_train_df[num_features].copy()
    new_X_test_num = new_test_df[num_features].copy()

    #scaler = StandardScaler()

    new_X_train_num = scaler.fit_transform(new_X_train_num)
    new_X_test_num = scaler.transform(new_X_test_num)

    # итоговые датасеты для обучения
    new_X_train = np.hstack((new_X_train_cat, new_X_train_num))
    new_X_test = np.hstack((new_X_test_cat, new_X_test_num))

    # сохраним это в файлы
    np.savetxt('data/new_X_train.txt', new_X_train)
    np.savetxt('data/new_X_test.txt', new_X_test)
    np.savetxt('data/new_y_train.txt', new_y_train)
    np.savetxt('data/new_y_test.txt', new_y_test)
    
    return X_train, X_test, y_train, y_test, new_X_train, new_X_test, new_y_train, new_y_test

